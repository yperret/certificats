<?php

/*
  Auteur : Yannick Perret (yannick.perret@liris.cnrs.fr) pour le LIRIS (https://liris.cnrs.fr)

  Licence : GPL.
  Réutilisation libre, à vos risque. Merci de m'en informer.
 */

// pour error() et entete()
require "tools.php";


// le répertoire est protégé par une authentification (LDAP dans notre cas)
$login = $_SERVER['PHP_AUTH_USER'];
if (empty($login)) {
  error("échec de récupération de votre login. Contactez XXXXX");
}

// on récupère les infos ($password2 est le mot de passe d'export : on utilise le même)
$password = $_POST['password'];
$password2 = $password;
$key = $_POST['takey'];
$cert = $_POST['tacrt'];
$ca = $_POST['taca'];

// vérification des données
if (empty($password) || empty($password2) || empty($key) ||
    empty($cert)) {
  error("il manque une information requise (clé privée, certificat ou mot de passe)");
}


// on charge la clé
$skey = openssl_pkey_get_private($key, $password);
if ($skey === FALSE) {
  error("échec de lecture de la clé (mauvais fichier ? mauvais mot de passe ?)");
}

// on fait la conversion du certificat
$ret = openssl_pkcs12_export($cert, $cres, $skey, $password2);
if ($ret === FALSE) {
  error("échec de convertion du certificat (mauvais fichier ?)");
}

// ok. zone pour récupérer (sauvegarder) le fichier P12 (qui est binaire, d'où le passage
// par du BASE64 en intermédiaire)
// le fichier est nommé par défaut selon le login de l'utilisateur
head("Conversion de certificat");
echo <<<_EOT_
<p>La conversion a fonctionné. Récupérez le certificat au format P12
en utilisant le bouton « Télécharger » ci-dessous.</p>
<p>Veuillez à ne pas oublier le mot de passe associé, il vous sera nécessaire pour
importer votre certificat dans un outil (navigateur…).</p>
<p>Vous référer à la documentation pour l'import d'un certificat :
<a href='https://INSEREZ.VOTRE.DOC'>https://INSEREZ.VOTRE.DOC</a></p>
<a id='a' class='btn' download='$login.p12' type='application/octet-stream'>Télécharger le certificat format P12</a>
_EOT_;

// partie pour encoder le contenu ($cres) en base64 (on ne peut pas mettre du binaire directement
//  dans le code HTML) puis inversement, et permet le téléchargement
echo <<<EOF
<script>
/**
 * Convert a base64 string to an ArrayBuffer
 */
function base64ToArrayBuffer( base64 ) {
    var raw = window.atob( base64 );
    var rawLength = raw.length;
    var array = new Uint8Array( new ArrayBuffer(rawLength) );

    for( i = 0; i < rawLength; i++ ) {
        array[ i ] = raw.charCodeAt( i );
    }
    return( array.buffer );
}
EOF;
echo "var a = document.getElementById('a'); var cont = base64ToArrayBuffer('";
echo base64_encode($cres);
echo "'); var bcont = new Blob([cont]); a.href = URL.createObjectURL(bcont);</script>\n";

echo "</body>\n</html>\n";

die();
