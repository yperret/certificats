<?php


// génère un document "en erreur"
function error($text) {
  echo <<<_EOT_
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Erreur</title>
  <meta name="Description" content="Erreur">  
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="icon" type="type/ico" href="HAL_favicon.ico" />
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<p>Erreur : $text.</p>
</body>
</html>
_EOT_;
  die(1);
}

// génère l'entête HTML
function head($title) {
  echo <<<_EOT_
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>$title</title>
  <meta name="Description" content="Résultat">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="icon" type="type/ico" href="HAL_favicon.ico" />
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
_EOT_;
}
