Ce code a pour but la conversion d'une paire « clé privée » et « certificat PEM » en un certificat
format P12 (PKCS12), en gérant les mots de passe.

# Pourquoi ?

Parce que les certificats reçus via DigiCert (certificats personnels) sont au format PEM, et que
pour un import dans un navigateur ou un gestionnaire de mail il faut un format P12, et surtout
que la conversion nécessite de lancer des commandes `openssl` dans un terminal (difficile pour les
personnes sous windows, et compliqué pour celles n'ayant pas l'habitude de la ligne de commande).

Et parce qu'il vaut mieux passer par un outil « corporate » que par un quelconque outil trouvé sur
le WEB, car il faut donner sa clé privée.

# Comment ?

L'API WEB est codée en `PHP`. Elle utilise l'extension `openssl` de `PHP`. Les saisies (certificat
à convertir) passent par un _upload_ (petit script `JS`). La récupération passe par un téléchargement.
Afin d'éviter d'avoir à stocker le résultat (certificat P12) sur le serveur le téléchargement
se fait à partir des données du script sans passer par le disque (script `JS`).

# Contenu

 * `tools.php` : outils. `error()` génère une page WEB d'erreur, avec le message indiqué (et quitte). `entete()` génère une entête HTML.
 * `p12.php` : formulaire de saisie des informations. Le formulaire demande le mot de passe, la clé
 privée et le certificat, et effectue un `POST` vers `vp12.php`
 * `vp12.php` : valide les informations reçues, puis effectue les conversions vers le format P12 en
 utilisant l'extension `openssl`. Au final propose de télécharger le résultat

# Technique

## Zones de saisie / upload de données

Ce sont des `textarea` sur lesquelles un bouton (en fait un `input` de type `file`) permet de lancer
une sélection et un chargement de fichier.

Voir dans le `JS` la fonction `getFile()` (associé à l'événement `change` de l'`input` `file`) qui utilise
la fonction `placeFileContent()` qui utilise `readFileContent()`.

## Zone de récupération / download du résultat

Le résultat (format P12) étant binaire, il n'est pas possible de passer par une `textarea`.

La récupération passe par un lien (`a`, avec le type `application/octet-stream`, et dont l'aspect est « maquillé »
en bouton via la classe `btn`). Le script `JS` qui suit crée un `Blob` à partir des données du certificat P12,
et fait pointer le `href` du `a` dessus (URL virtuelle).

La création du `Blob` passe par une conversion de/vers le format base64, car il n'est pas possible de mettre
une chaîne binaire dans du code `HTML` / `JS`.

## Commandes openssl

La conversion utilise l'extension `openssl`. Les commandes :
 * `openssl_pkey_get_private($key, $password)` : charge la clé (`$key`, format PEM) protégée par le mot de passe
 `$password` et retourne une « ressource de clé » (format anonyme interne à `openssl`)
 * `openssl_pkcs12_export($cert, $cres, $skey, $password2)` : exporte le certificat PEM `$cert` et sa clé associée
 `$skey` (voir juste au dessus) au format P12 (PKCS12) en le protégeant avec le mot de passe `$password2`. Le
 résultat (binaire, format P12) est rangé dans `$cres`

## Général

Cet outil utilise l'authentification Apache via le LDAP du LIRIS. La vérification du _login_ n'est pas strictement
nécessaire. Toutefois ça permet :
 * de limiter l'accès aux membres du laboratoire (ce n'est pas un outil public actuellement)
 * de pouvoir proposer un nom de fichier pour la récupération du résultat (`$login.p12`)

# Licence

Copyright Yannick Perret / LIRIS, 2019-2020.
GPLv3. Ce code est réutilisable, sans garantie quelconque (à vos risques et périls… :)). Si vous réutilisez ce code
merci de m'en informer (yannick.perret@liris.cnrs.fr).


